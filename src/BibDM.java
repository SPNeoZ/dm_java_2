import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer min = null;
        for (Integer elem : liste)
        {
            if (min == null)
                min = elem;
            else
                if (elem < min)
                {
                    min = elem;
                }
        }
        return min;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeur donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        boolean plusPetit = true;
        int i = 0;
        while (plusPetit && i < liste.size())
        {
            if (valeur.compareTo(liste.get(i)) >= 0)
                plusPetit = false;
            i++;
        }
        return plusPetit;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> intersection = new ArrayList<>();
        int i = 0; // i indice de l'élément de liste1
        int y = 0; // y indice de l'élément de liste2
        while (i < liste1.size() && y < liste2.size())
        {
            if (liste1.get(i).equals(liste2.get(y)))
            {
                if (!intersection.contains(liste1.get(i)))
                    intersection.add(liste1.get(i));
                i++;
                y++;
            }
            else
                if (liste1.get(i).compareTo(liste2.get(y)) > 0)
                    y++;
                else
                    i++;
        }
        return intersection;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> liste_mots = new ArrayList<>();
        String[] mots = texte.split(" ");
        for (int i=0; i<mots.length; i++)
            if (!mots[i].equals(""))
                liste_mots.add(mots[i]);
        return liste_mots;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */
    public static String motMajoritaire(String texte){
        List<String> liste_mots = decoupe(texte);
        String motMaj;
        int nbApparitions;
        if (liste_mots.size() != 0)
        {
            motMaj = liste_mots.get(0);
            nbApparitions = Collections.frequency(liste_mots, motMaj);
        }
        else
        {
            motMaj = null;
            nbApparitions = 0;
        }
        for (int i=1; i<liste_mots.size(); i++)
        {
            String mot = liste_mots.get(i);
            int frequence_mot = Collections.frequency(liste_mots, mot);
            if (frequence_mot > nbApparitions)
            {
                motMaj = mot;
                nbApparitions = frequence_mot;
            }
            if (frequence_mot == nbApparitions)
                if (mot.compareTo(motMaj) < 1)
                {
                    motMaj = mot;
                    nbApparitions = frequence_mot;
                }
        }
        return motMaj;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int nb_par_fermantes = 0;
        int nb_par_ouvrantes = 0;
        for (int i=0; i<chaine.length(); i++)
        {
            if (chaine.charAt(i) == ')')
                    nb_par_fermantes++;
            if (chaine.charAt(i) == '(')
                nb_par_ouvrantes++;
            if (nb_par_fermantes > nb_par_ouvrantes)
                return false;
        }
        return (nb_par_fermantes == nb_par_ouvrantes);
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        List<Character> ouvrants = new ArrayList<>();
        for (int i=0; i<chaine.length(); i++)
        {
            char car = chaine.charAt(i);
            if (car == '(' || car == '[')
                ouvrants.add(car);
            else
            {
                if (car == ')')
                {
                    if (ouvrants.size() > 0 && ouvrants.get(ouvrants.size()-1).equals('('))
                        ouvrants.remove(ouvrants.size()-1);
                    else
                        return false;
                }
                else
                {
                    if (car == ']')
                    {
                        if (ouvrants.size() > 0 && ouvrants.get(ouvrants.size()-1).equals('['))
                            ouvrants.remove(ouvrants.size()-1);
                        else
                            return false;
                    }
                }
            }
        }
        return (ouvrants.size() == 0);
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        int debut = 0;
        int fin = liste.size()-1;
        int milieu;
        while (fin >= debut)
        {
            milieu = (debut + fin) / 2;
            if (liste.get(milieu) == valeur)
                return true;
            if (liste.get(milieu) > valeur)
                fin = milieu - 1;
            else
                debut = milieu + 1;
        }
        return false;
    }



}
